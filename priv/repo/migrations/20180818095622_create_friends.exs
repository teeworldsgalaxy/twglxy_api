defmodule TwglxyApi.Repo.Migrations.CreateFriends do
  use Ecto.Migration

  def change do
    create table(:friends) do
      add :status, :string
      timestamps()
    end

  end
end
