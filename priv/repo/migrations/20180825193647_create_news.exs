defmodule TwglxyApi.Repo.Migrations.CreateNews do
  use Ecto.Migration

  def change do
    create table(:news) do
      add :content, :string
      add :headline, :string
      add :author, :string
      add :date, :utc_datetime

      timestamps()
    end

  end
end
