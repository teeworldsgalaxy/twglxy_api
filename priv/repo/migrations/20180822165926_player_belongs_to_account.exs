defmodule TwglxyApi.Repo.Migrations.PlayerBelongsToAccount do
  use Ecto.Migration

  def change do
    alter table(:players) do
      add :account_id, references(:accounts)
    end
  end
end
