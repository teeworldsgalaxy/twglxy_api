defmodule TwglxyApi.Repo.Migrations.CreateMessages do
  use Ecto.Migration

  def change do
    create table(:messages) do
      add :sent_at, :utc_datetime
      add :text, :string
      add :author_name, :string
      add :author_id, :integer

      timestamps()
    end

  end
end
