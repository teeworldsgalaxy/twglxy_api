defmodule TwglxyApi.Repo.Migrations.FriendBelongsToPlayerAndFriend do
  use Ecto.Migration

  def change do
    alter table(:friends) do
      add :player_id, references(:players)
      add :friend_id, references(:players)
    end
  end
end
