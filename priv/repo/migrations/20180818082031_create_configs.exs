defmodule TwglxyApi.Repo.Migrations.CreateConfigs do
  use Ecto.Migration

  def change do
    create table(:configs) do
      add :name, :string
      add :data, :map

      timestamps()
    end

  end
end
