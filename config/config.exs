# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :twglxy_api,
  ecto_repos: [TwglxyApi.Repo]

# Configures the endpoint
config :twglxy_api, TwglxyApiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "mTXq0S5yo7a0F0XzzU3ThyO29IOozydIcD3PRf8ge8TXeBo7fV7FWmk1xyLuFrgO",
  render_errors: [view: TwglxyApiWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: TwglxyApi.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

config :twglxy_api, TwglxyApi.Guardian,
  issuer: "TwglxyApi",
  secret_key: "l2gLzn+bksZ4eqPU4xFkBfHeNGfBv7i+TKqxADcns1ecGwsPotIL9IpGShx7ctgA"

config :twglxy_api, Mix.Tasks.Twglxy.RegenerateUpdaterCache,
  base_path: System.get_env("TWGLXY_CLIENT_VERSIONS_DIR")

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
