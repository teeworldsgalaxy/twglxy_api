defmodule TwglxyApiWeb.ConfigControllerTest do
  use TwglxyApiWeb.ConnCase

  alias TwglxyApi.Players
  alias TwglxyApi.Players.Config

  @create_attrs %{data: %{}, name: "some name"}
  @update_attrs %{data: %{}, name: "some updated name"}
  @invalid_attrs %{data: nil, name: nil}

  def fixture(:config) do
    {:ok, config} = Players.create_config(@create_attrs)
    config
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all configs", %{conn: conn} do
      conn = get conn, config_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create config" do
    test "renders config when data is valid", %{conn: conn} do
      conn = post conn, config_path(conn, :create), config: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, config_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "data" => %{},
        "name" => "some name"}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, config_path(conn, :create), config: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update config" do
    setup [:create_config]

    test "renders config when data is valid", %{conn: conn, config: %Config{id: id} = config} do
      conn = put conn, config_path(conn, :update, config), config: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, config_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "data" => %{},
        "name" => "some updated name"}
    end

    test "renders errors when data is invalid", %{conn: conn, config: config} do
      conn = put conn, config_path(conn, :update, config), config: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete config" do
    setup [:create_config]

    test "deletes chosen config", %{conn: conn, config: config} do
      conn = delete conn, config_path(conn, :delete, config)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, config_path(conn, :show, config)
      end
    end
  end

  defp create_config(_) do
    config = fixture(:config)
    {:ok, config: config}
  end
end
