defmodule TwglxyApiWeb.NewsEntryControllerTest do
  use TwglxyApiWeb.ConnCase

  alias TwglxyApi.News
  alias TwglxyApi.News.NewsEntry

  @create_attrs %{author: "some author", content: "some content", date: "2010-04-17 14:00:00.000000Z"}
  @update_attrs %{author: "some updated author", content: "some updated content", date: "2011-05-18 15:01:01.000000Z"}
  @invalid_attrs %{author: nil, content: nil, date: nil}

  def fixture(:news_entry) do
    {:ok, news_entry} = News.create_news_entry(@create_attrs)
    news_entry
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all news", %{conn: conn} do
      conn = get conn, news_entry_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create news_entry" do
    test "renders news_entry when data is valid", %{conn: conn} do
      conn = post conn, news_entry_path(conn, :create), news_entry: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, news_entry_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "author" => "some author",
        "content" => "some content",
        "date" => "2010-04-17 14:00:00.000000Z"}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, news_entry_path(conn, :create), news_entry: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update news_entry" do
    setup [:create_news_entry]

    test "renders news_entry when data is valid", %{conn: conn, news_entry: %NewsEntry{id: id} = news_entry} do
      conn = put conn, news_entry_path(conn, :update, news_entry), news_entry: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, news_entry_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "author" => "some updated author",
        "content" => "some updated content",
        "date" => "2011-05-18 15:01:01.000000Z"}
    end

    test "renders errors when data is invalid", %{conn: conn, news_entry: news_entry} do
      conn = put conn, news_entry_path(conn, :update, news_entry), news_entry: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete news_entry" do
    setup [:create_news_entry]

    test "deletes chosen news_entry", %{conn: conn, news_entry: news_entry} do
      conn = delete conn, news_entry_path(conn, :delete, news_entry)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, news_entry_path(conn, :show, news_entry)
      end
    end
  end

  defp create_news_entry(_) do
    news_entry = fixture(:news_entry)
    {:ok, news_entry: news_entry}
  end
end
