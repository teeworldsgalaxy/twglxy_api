defmodule TwglxyApi.NewsTest do
  use TwglxyApi.DataCase

  alias TwglxyApi.News

  describe "news" do
    alias TwglxyApi.News.NewsEntry

    @valid_attrs %{author: "some author", content: "some content", date: "2010-04-17 14:00:00.000000Z"}
    @update_attrs %{author: "some updated author", content: "some updated content", date: "2011-05-18 15:01:01.000000Z"}
    @invalid_attrs %{author: nil, content: nil, date: nil}

    def news_entry_fixture(attrs \\ %{}) do
      {:ok, news_entry} =
        attrs
        |> Enum.into(@valid_attrs)
        |> News.create_news_entry()

      news_entry
    end

    test "list_news/0 returns all news" do
      news_entry = news_entry_fixture()
      assert News.list_news() == [news_entry]
    end

    test "get_news_entry!/1 returns the news_entry with given id" do
      news_entry = news_entry_fixture()
      assert News.get_news_entry!(news_entry.id) == news_entry
    end

    test "create_news_entry/1 with valid data creates a news_entry" do
      assert {:ok, %NewsEntry{} = news_entry} = News.create_news_entry(@valid_attrs)
      assert news_entry.author == "some author"
      assert news_entry.content == "some content"
      assert news_entry.date == DateTime.from_naive!(~N[2010-04-17 14:00:00.000000Z], "Etc/UTC")
    end

    test "create_news_entry/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = News.create_news_entry(@invalid_attrs)
    end

    test "update_news_entry/2 with valid data updates the news_entry" do
      news_entry = news_entry_fixture()
      assert {:ok, news_entry} = News.update_news_entry(news_entry, @update_attrs)
      assert %NewsEntry{} = news_entry
      assert news_entry.author == "some updated author"
      assert news_entry.content == "some updated content"
      assert news_entry.date == DateTime.from_naive!(~N[2011-05-18 15:01:01.000000Z], "Etc/UTC")
    end

    test "update_news_entry/2 with invalid data returns error changeset" do
      news_entry = news_entry_fixture()
      assert {:error, %Ecto.Changeset{}} = News.update_news_entry(news_entry, @invalid_attrs)
      assert news_entry == News.get_news_entry!(news_entry.id)
    end

    test "delete_news_entry/1 deletes the news_entry" do
      news_entry = news_entry_fixture()
      assert {:ok, %NewsEntry{}} = News.delete_news_entry(news_entry)
      assert_raise Ecto.NoResultsError, fn -> News.get_news_entry!(news_entry.id) end
    end

    test "change_news_entry/1 returns a news_entry changeset" do
      news_entry = news_entry_fixture()
      assert %Ecto.Changeset{} = News.change_news_entry(news_entry)
    end
  end
end
