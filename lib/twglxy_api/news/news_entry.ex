defmodule TwglxyApi.News.NewsEntry do
  use Ecto.Schema
  import Ecto.Changeset


  schema "news" do
    field :author, :string
    field :content, :string
    field :headline, :string
    field :date, :utc_datetime

    timestamps()
  end

  @doc false
  def changeset(news_entry, attrs) do
    news_entry
    |> cast(attrs, [:content, :author, :date, :headline])
    |> validate_required([:content, :author, :date, :headline])
  end
end
