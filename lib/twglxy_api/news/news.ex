defmodule TwglxyApi.News do
  @moduledoc """
  The News context.
  """

  import Ecto.Query, warn: false
  alias TwglxyApi.Repo

  alias TwglxyApi.News.NewsEntry

  @doc """
  Returns the list of news.

  ## Examples

      iex> list_news()
      [%NewsEntry{}, ...]

  """
  def list_news do
    Repo.all(NewsEntry)
  end

  @doc """
  Gets a single news_entry.

  Raises `Ecto.NoResultsError` if the News entry does not exist.

  ## Examples

      iex> get_news_entry!(123)
      %NewsEntry{}

      iex> get_news_entry!(456)
      ** (Ecto.NoResultsError)

  """
  def get_news_entry!(id), do: Repo.get!(NewsEntry, id)

  @doc """
  Creates a news_entry.

  ## Examples

      iex> create_news_entry(%{field: value})
      {:ok, %NewsEntry{}}

      iex> create_news_entry(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_news_entry(attrs \\ %{}) do
    %NewsEntry{}
    |> NewsEntry.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a news_entry.

  ## Examples

      iex> update_news_entry(news_entry, %{field: new_value})
      {:ok, %NewsEntry{}}

      iex> update_news_entry(news_entry, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_news_entry(%NewsEntry{} = news_entry, attrs) do
    news_entry
    |> NewsEntry.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a NewsEntry.

  ## Examples

      iex> delete_news_entry(news_entry)
      {:ok, %NewsEntry{}}

      iex> delete_news_entry(news_entry)
      {:error, %Ecto.Changeset{}}

  """
  def delete_news_entry(%NewsEntry{} = news_entry) do
    Repo.delete(news_entry)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking news_entry changes.

  ## Examples

      iex> change_news_entry(news_entry)
      %Ecto.Changeset{source: %NewsEntry{}}

  """
  def change_news_entry(%NewsEntry{} = news_entry) do
    NewsEntry.changeset(news_entry, %{})
  end
end
