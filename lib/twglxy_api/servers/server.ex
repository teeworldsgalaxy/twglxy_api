defmodule TwglxyApi.Servers.Server do
  use Ecto.Schema
  import Ecto.Changeset


  schema "servers" do
    field :name, :string
    field :ip_address, EctoFields.IP

    timestamps()
  end

  @doc false
  def changeset(server, attrs) do
    server
    |> cast(attrs, [:name])
    |> validate_required([:name,:ip_address])
  end
end
