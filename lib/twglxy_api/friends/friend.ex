defmodule TwglxyApi.Friends.Friend do
  use Ecto.Schema
  import Ecto.Changeset

  alias TwglxyApi.Players


  schema "friends" do
    field :status, :string
    belongs_to :friend, Players.Player
    belongs_to :player, Players.Player
    timestamps()
  end

  @doc false
  def changeset(friend, attrs) do
    friend
    |> cast(attrs, [:status, :player_id, :friend_id])
    |> validate_required([:status])
    |> assoc_constraint(:friend)
    |> assoc_constraint(:player)
  end
end
