defmodule TwglxyApi.Chats.Conversation do
  use Ecto.Schema
  import Ecto.Changeset


  schema "conversations" do

    timestamps()
  end

  @doc false
  def changeset(conversation, attrs) do
    conversation
    |> cast(attrs, [])
    |> validate_required([])
  end
end
