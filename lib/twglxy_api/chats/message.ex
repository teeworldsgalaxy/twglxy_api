defmodule TwglxyApi.Chats.Message do
  use Ecto.Schema
  import Ecto.Changeset


  schema "messages" do
    field :author_id, :integer
    field :author_name, :string
    field :sent_at, :utc_datetime
    field :text, :string

    timestamps()
  end

  @doc false
  def changeset(message, attrs) do
    message
    |> cast(attrs, [:sent_at, :text, :author_name, :author_id])
    |> validate_required([:sent_at, :text, :author_name, :author_id])
  end
end
