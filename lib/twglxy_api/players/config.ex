defmodule TwglxyApi.Players.Config do
  use Ecto.Schema
  import Ecto.Changeset


  schema "configs" do
    field :data, :map
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(config, attrs) do
    config
    |> cast(attrs, [:name, :data])
    |> validate_required([:name, :data])
  end
end
