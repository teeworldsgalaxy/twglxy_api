defmodule TwglxyApi.Players.Player do
  use Ecto.Schema
  import Ecto.Changeset

  alias TwglxyApi.Accounts


  schema "players" do
    field :name, :string
    belongs_to :account, Accounts.Account
    timestamps()
  end

  @doc false
  def changeset(player, attrs) do
    player
    |> cast(attrs, [:name, :account_id])
    |> validate_required([:name])
    |> unique_constraint(:name)
    |> assoc_constraint(:account)
  end
end
