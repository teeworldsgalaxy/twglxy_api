defmodule TwglxyApi.Accounts do
  @moduledoc """
  The Accounts context.
  """

  alias TwglxyApi.Guardian
  import Comeonin.Bcrypt, only: [checkpw: 2, dummy_checkpw: 0]

  import Ecto.Query, warn: false
  alias TwglxyApi.Repo

  alias TwglxyApi.Accounts.Account

  @doc """
  Returns the list of accounts.

  ## Examples

      iex> list_accounts()
      [%Account{}, ...]

  """
  def list_accounts do
    Repo.all(Account)
  end

  @doc """
  Gets a single account.

  Raises `Ecto.NoResultsError` if the Account does not exist.

  ## Examples

      iex> get_account!(123)
      %Account{}

      iex> get_account!(456)
      ** (Ecto.NoResultsError)

  """
  def get_account!(id), do: Repo.get!(Account, id)

  @doc """
  Creates a account.

  ## Examples

      iex> create_account(%{field: value})
      {:ok, %Account{}}

      iex> create_account(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_account(attrs \\ %{}) do
    %Account{}
    |> Account.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a account.

  ## Examples

      iex> update_account(account, %{field: new_value})
      {:ok, %Account{}}

      iex> update_account(account, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_account(%Account{} = account, attrs) do
    account
    |> Account.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Account.

  ## Examples

      iex> delete_account(account)
      {:ok, %Account{}}

      iex> delete_account(account)
      {:error, %Ecto.Changeset{}}

  """
  def delete_account(%Account{} = account) do
    Repo.delete(account)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking account changes.

  ## Examples

      iex> change_account(account)
      %Ecto.Changeset{source: %Account{}}

  """
  def change_account(%Account{} = account) do
    Account.changeset(account, %{})
  end



  def token_sign_in(%Account{} = account, password) do
    case verify_password(password, account) do
      {:ok, account} ->
        Guardian.encode_and_sign(account)
      _ ->
        {:error, :unauthorized}
    end
  end

  def token_sign_in(email, password) do
    case email_password_auth(email, password) do
      {:ok, account} ->
        Guardian.encode_and_sign(account)
      _ ->
        {:error, :unauthorized}
    end
  end

  defp email_password_auth(email, password) when is_binary(email) and is_binary(password) do
    with {:ok, account} <- get_by_email(email),
    do: verify_password(password, account)
  end

  defp get_by_email(email) when is_binary(email) do
    case Repo.get_by(Account, email: email) do
      nil ->
        dummy_checkpw()
        {:error, "Login error."}
      account ->
        {:ok, account}
    end
  end

  defp verify_password(password, %Account{} = account) when is_binary(password) do
    if checkpw(password, account.password_hash) do
      {:ok, account}
    else
      {:error, :invalid_password}
    end
  end
end
