defmodule TwglxyApiWeb.ConfigController do
  use TwglxyApiWeb, :controller

  alias TwglxyApi.Players
  alias TwglxyApi.Players.Config

  action_fallback TwglxyApiWeb.FallbackController

  def index(conn, _params) do
    configs = Players.list_configs()
    render(conn, "index.json", configs: configs)
  end

  def create(conn, %{"config" => config_params}) do
    with {:ok, %Config{} = config} <- Players.create_config(config_params) do
      conn
      |> put_status(:created)
      #|> put_resp_header("location", config_path(conn, :show, config))
      |> render("show.json", config: config)
    end
  end

  def show(conn, %{"id" => id}) do
    config = Players.get_config!(id)
    render(conn, "show.json", config: config)
  end

  def update(conn, %{"id" => id, "config" => config_params}) do
    config = Players.get_config!(id)

    with {:ok, %Config{} = config} <- Players.update_config(config, config_params) do
      render(conn, "show.json", config: config)
    end
  end

  def delete(conn, %{"id" => id}) do
    config = Players.get_config!(id)
    with {:ok, %Config{}} <- Players.delete_config(config) do
      send_resp(conn, :no_content, "")
    end
  end
end
