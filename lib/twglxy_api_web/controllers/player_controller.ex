defmodule TwglxyApiWeb.PlayerController do
  use TwglxyApiWeb, :controller

  alias TwglxyApi.Players
  alias TwglxyApi.Players.Player
  alias TwglxyApi.Repo

  import Ecto.Query

  action_fallback TwglxyApiWeb.FallbackController

  def index(conn, _params) do
    players = Players.list_players()
    render(conn, "index.json", players: players)
  end

  def show(conn, %{"id" => id}) do
    player = Players.get_player!(id)
    render(conn, "player.json", player: player)
  end

  def search(conn, %{"term" => term}) do
    player = Players.current_player!(conn)
    sanitized_term = String.replace(term, "%", "\\%")
    players = (from p in Player,
              where: ilike(p.name, ^"%#{sanitized_term}%") and p.id != ^player.id)
              |> Repo.all()
    render(conn, "search.json", players: players)
  end
end
