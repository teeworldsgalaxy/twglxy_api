defmodule TwglxyApiWeb.NewsEntryController do
  use TwglxyApiWeb, :controller

  alias TwglxyApi.News
  alias TwglxyApi.News.NewsEntry

  action_fallback TwglxyApiWeb.FallbackController

  def index(conn, _params) do
    news = News.list_news()
    render(conn, "index.json", news: news)
  end
end
