defmodule TwglxyApiWeb.UpdaterController do
  use TwglxyApiWeb, :controller

  alias TwglxyApi.Accounts
  alias TwglxyApi.Accounts.Account
  alias TwglxyApi.Guardian
  alias TwglxyApi.Players
  alias TwglxyApi.Players.Player
  alias TwglxyApi.Repo
  alias Ecto.Multi
  alias Mix.Tasks.Twglxy.RegenerateUpdaterCache

  action_fallback TwglxyApiWeb.FallbackController

  def pretty_json(conn, data) do
    conn
    |> Plug.Conn.put_resp_header("content-type", "application/json; charset=utf-8")
    |> Plug.Conn.send_resp(200, Poison.encode!(data, pretty: true))
  end


  def get_os(name) do
    case name do
      "win" <> _ -> "win32"
      "linux" <> _ -> "linux"
      _ -> "osx"
    end
  end

  def version_info(conn, %{"version" => version, "os" => os}) do
    IO.inspect(version)

    os = get_os(os)
    data = 
      case version do
        "current" -> current = RegenerateUpdaterCache.get_cache() |> hd
                      with %{"version" => version, "files" => %{^os => os_files}} <- current do
                        %{version: version, files: os_files}
                      end
        x -> {:error, "Unsupported version"}
      end
    IO.inspect(data)
    pretty_json(conn, data)
  end


  def sanitize_path(path) do
    path
      |> String.replace("..", "")
      |> String.replace("~", "")
  end

  def download_file(conn, %{"version" => version, "os" => os, "file" => file}) do
    base_path = RegenerateUpdaterCache.base_path()
    os = get_os(os)
    path = sanitize_path(base_path <> "#{version}/#{os}/#{file}")
    send_file(conn, 200, path)
  end
end