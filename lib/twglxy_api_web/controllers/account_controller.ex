defmodule TwglxyApiWeb.AccountController do
  use TwglxyApiWeb, :controller

  alias TwglxyApi.Accounts
  alias TwglxyApi.Accounts.Account
  alias TwglxyApi.Guardian
  alias TwglxyApi.Players
  alias TwglxyApi.Players.Player
  alias TwglxyApi.Repo
  alias Ecto.Multi

  import Ecto.Query

  action_fallback TwglxyApiWeb.FallbackController

  def index(conn, _params) do
    accounts = Accounts.list_accounts()
    render(conn, "index.json", accounts: accounts)
  end


  def register(conn, %{"email" => email, "password" => password, "name" => name}) do

    account_params = %{"email" => email, "password" => password}
    player_params = %{"name" => name}

    with {:ok, %{:account => %Account{} = account, :player => %Player{} = player}} <- 
        Multi.new
        |> Multi.insert(:account, Account.changeset(%Account{}, account_params))
        |> Multi.run(:player, fn %{:account => account} ->
          # Use the inserted team.
          player_changeset = Player.changeset(%Player{}, Map.put(player_params, "account_id", account.id) |> IO.inspect)
            |> Repo.insert()
        end)
        |> Repo.transaction(), 
      {:ok, token, _claims} <- Guardian.encode_and_sign(account) do
      conn
        |> render("register.json", %{jwt: token, player: player, account: account})
    else
      {:error, _, changeset, _} -> {:error, changeset}
    end
  end

  def login(conn, %{"email" => email, "password" => password} = params) do
    account = Repo.get_by(Accounts.Account, email: email)
    player = Repo.get_by(Players.Player, account_id: account.id)

    case Accounts.token_sign_in(email, password) do
      {:ok, token, _claims} ->
        conn |> render("login.json", %{jwt: token, player: player, account: account})
      _ ->
        {:error, :unauthorized}
    end
  end

  def login(conn, %{"name" => name, "password" => password}) do
    player = Repo.get_by(Players.Player, name: name)
              |> Repo.preload(:account)
    account = player.account

    case Accounts.token_sign_in(account, password) do
      {:ok, token, _claims} ->
        conn |> render("login.json", %{jwt: token, player: player, account: account})
      _ ->
        {:error, :unauthorized}
    end
  end

  def login_check(conn, _params) do
    account = Guardian.Plug.current_resource(conn)
    player = Repo.get_by(Players.Player, account_id: account.id)
    conn |> render("login_check.json", %{player: player, account: account})
  end

  # Not used
  def create(conn, %{"account" => account_params}) do
    with {:ok, %Account{} = account} <- Accounts.create_account(account_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", account_path(conn, :show, account))
      |> render("show.json", account: account)
    end
  end

  def show(conn, %{"id" => id}) do
    account = Accounts.get_account!(id)
    render(conn, "show.json", account: account)
  end

  def show_own(conn, _) do
    account = Guardian.Plug.current_resource(conn)
    conn |> render("account.json", account: account)
  end

  def update(conn, %{"id" => id, "account" => account_params}) do
    account = Accounts.get_account!(id)

    with {:ok, %Account{} = account} <- Accounts.update_account(account, account_params) do
      render(conn, "show.json", account: account)
    end
  end

  def delete(conn, %{"id" => id}) do
    account = Accounts.get_account!(id)
    with {:ok, %Account{}} <- Accounts.delete_account(account) do
      send_resp(conn, :no_content, "")
    end
  end
end
