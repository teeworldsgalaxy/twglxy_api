defmodule TwglxyApiWeb.FriendController do
  use TwglxyApiWeb, :controller

  alias TwglxyApi.Friends
  alias TwglxyApi.Friends.Friend
  alias TwglxyApi.Players
  alias TwglxyApi.Players.Player
  alias TwglxyApi.Repo

  action_fallback TwglxyApiWeb.FallbackController

  def list(conn, _params) do
    player = Players.current_player!(conn)
    accepted = Friends.list_accepted(player)
    requested = Friends.list_requested(player)
    requesting = Friends.list_requesting(player)
    render(conn, "list.json", %{
      accepted: accepted,
      requested: requested,
      requesting: requesting
    })
  end


  def add(conn, %{"friend" => friend_id} = params) do
    player = Players.current_player!(conn)
    friend_player = Players.get_player!(friend_id)

    Repo.transaction(fn ->
      # check if other friend entry is existing and set to accepted
      false = friend_id == player.id
      Friends.remove(player, friend_player)
      friend_entry = Repo.get_by(Friends.Friend, %{friend_id: player.id, player_id: friend_id})
      status = with %Friend{} <- friend_entry do
                  Friends.update_friend(friend_entry, %{status: "accepted"})
                  "accepted"
              else
                _ -> "pending"
              end
      # Add Friend entry
      {:ok, %Friend{} = friend} =
        Friends.create_friend(%{
          friend_id: friend_id,
          player_id: player.id,
          status: status
        })
    end)
    list(conn, %{})
  end

  def remove(conn, %{"friend" => friend_id}) do
    player = Players.current_player!(conn)
    friend_player = Players.get_player!(friend_id)

    # remove both entries
    Repo.transaction(fn ->
      Friends.remove(player, friend_player)
      Friends.remove(friend_player, player)
    end)
    list(conn, %{})  # Return list
  end
end
