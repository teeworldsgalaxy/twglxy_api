defmodule TwglxyApi.Guardian.AuthPipeline do
    use Guardian.Plug.Pipeline, otp_app: :twlgxy_api,
    module: TwglxyApi.Guardian,
    error_handler: TwglxyApi.AuthErrorHandler
  
    plug Guardian.Plug.VerifyHeader, realm: "Bearer"
    plug Guardian.Plug.EnsureAuthenticated
    plug Guardian.Plug.LoadResource
  end