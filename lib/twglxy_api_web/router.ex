defmodule TwglxyApiWeb.Router do
  use TwglxyApiWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :jwt_authenticated do
    plug TwglxyApi.Guardian.AuthPipeline
  end

  scope "/", TwglxyApiWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  # Security stuff, login etc.
  scope "/api/v1", TwglxyApiWeb do
    pipe_through :api
    post "/accounts/register", AccountController, :register
    post "/accounts/login", AccountController, :login
    
    get "/updater/version_info/:version/:os", UpdaterController, :version_info
    get "/updater/files/:version/:os", UpdaterController, :download_file
    
  end
  
  
  scope "/api/v1", TwglxyApiWeb do
    pipe_through [:api, :jwt_authenticated]
    get "/accounts/login_check", AccountController, :login_check
    get "/accounts/own", AccountController, :show_own
  
    get "/players/search", PlayerController, :search
    get "/players/:id", PlayerController, :show
    get "/friends", FriendController, :list
    post "/friends/add", FriendController, :add
    post "/friends/remove", FriendController, :remove
    get "/news/entries", NewsEntryController, :index
  
  end
end
