defmodule TwglxyApiWeb.NewsEntryView do
  use TwglxyApiWeb, :view
  alias TwglxyApiWeb.NewsEntryView

  def render("index.json", %{news: news}) do
    render_many(news, NewsEntryView, "news_entry.json")
  end

  def render("show.json", %{news_entry: news_entry}) do
    %{data: render_one(news_entry, NewsEntryView, "news_entry.json")}
  end

  def render("news_entry.json", %{news_entry: news_entry}) do
    %{id: news_entry.id,
      content: news_entry.content,
      author: news_entry.author,
      date: news_entry.date,
      headline: news_entry.headline
    }
  end
end
