defmodule TwglxyApiWeb.AccountView do
  use TwglxyApiWeb, :view
  alias TwglxyApiWeb.AccountView

  def render("index.json", %{accounts: accounts}) do
    %{data: render_many(accounts, AccountView, "account.json")}
  end

  def render("show.json", %{account: account}) do
    %{data: render_one(account, AccountView, "account.json")}
  end

  def render("account.json", %{account: account}) do
    %{id: account.id,
      email: account.email,
      #password_hash: account.password_hash
    }
  end

  def render("login.json", %{jwt: jwt, player: player, account: account}) do
    %{token: jwt,
    account: render_one(account, __MODULE__, "account.json"),
    player: render_one(player, TwglxyApiWeb.PlayerView, "player.json")}
  end

  def render("login_check.json", %{player: player, account: account}) do
    %{
    account: render_one(account, __MODULE__, "account.json"),
    player: render_one(player, TwglxyApiWeb.PlayerView, "player.json")}
  end

  def render("register.json", %{jwt: jwt, player: player, account: account}) do
    %{token: jwt,
    account: render_one(account, __MODULE__, "account.json"),
    player: render_one(player, TwglxyApiWeb.PlayerView, "player.json")}
  end
end