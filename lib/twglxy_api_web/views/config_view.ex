defmodule TwglxyApiWeb.ConfigView do
  use TwglxyApiWeb, :view
  alias TwglxyApiWeb.ConfigView

  def render("index.json", %{configs: configs}) do
    %{data: render_many(configs, ConfigView, "config.json")}
  end

  def render("show.json", %{config: config}) do
    %{data: render_one(config, ConfigView, "config.json")}
  end

  def render("config.json", %{config: config}) do
    %{id: config.id,
      name: config.name,
      data: config.data}
  end
end
