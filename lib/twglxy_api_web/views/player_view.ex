defmodule TwglxyApiWeb.PlayerView do
  use TwglxyApiWeb, :view
  alias TwglxyApiWeb.PlayerView

  def render("player.json", %{player: player}) do
    %{id: player.id,
      name: player.name}
  end

  def render("search.json", %{players: players}) do
    render_many(players, PlayerView, "player.json")
  end
end
