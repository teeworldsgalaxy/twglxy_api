defmodule TwglxyApiWeb.MessageView do
  use TwglxyApiWeb, :view
  alias TwglxyApiWeb.MessageView

  def render("index.json", %{messages: messages}) do
    %{data: render_many(messages, MessageView, "message.json")}
  end

  def render("show.json", %{message: message}) do
    %{data: render_one(message, MessageView, "message.json")}
  end

  def render("message.json", %{message: message}) do
    %{id: message.id,
      sent_at: message.sent_at,
      text: message.text,
      author_name: message.author_name,
      author_id: message.author_id}
  end
end
