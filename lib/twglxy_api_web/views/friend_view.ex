defmodule TwglxyApiWeb.FriendView do
  use TwglxyApiWeb, :view
  alias TwglxyApiWeb.FriendView
  alias TwglxyApiWeb.FriendView

  alias TwglxyApi.Friends.Friend
  alias TwglxyApi.Players.Player

  def render("list.json", %{accepted: accepted, requested: requested, requesting: requesting}) do
    %{
      accepted: render_many(accepted, __MODULE__, "friend_detail.json"),
      requested: render_many(requested, __MODULE__, "friend_detail.json"),
      requesting: render_many(requesting, __MODULE__, "friend_detail.json"),
    } |> IO.inspect
  end

  def render("friend_detail.json", %{friend: %Friend{player: %Player{}=player}=friend}) do
    %{id: friend.id,
      #status: friend.status,
      player: render_one(friend.player, TwglxyApiWeb.PlayerView, "player.json")
    }
  end

  def render("friend_detail.json", %{friend: %Friend{friend: %Player{}=player_friend}=friend}) do
    %{id: friend.id,
      #status: friend.status,
      friend: render_one(player_friend, TwglxyApiWeb.PlayerView, "player.json")
    }
  end

  def render("friend_detail.json", %{friend: %Friend{}=friend}) do
    %{id: friend.id,
      status: friend.status
    }
  end
end
