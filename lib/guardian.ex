defmodule TwglxyApi.Guardian do
    use Guardian, otp_app: :twglxy_api
  
    def subject_for_token(account, _claims) do
      sub = to_string(account.id)
      {:ok, sub}
    end
  
    def subject_for_token(_, _) do
      {:error, :reason_for_error}
    end
  
    def resource_from_claims(claims) do
      id = claims["sub"]
      resource = TwglxyApi.Accounts.get_account!(id)
      {:ok,  resource}
    end
  
    def resource_from_claims(_claims) do
      {:error, :reason_for_error}
    end
  end