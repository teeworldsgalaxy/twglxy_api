defmodule Mix.Tasks.Twglxy.RegenerateUpdaterCache do
    use Mix.Task
  
    @shortdoc "Sends a greeting to us from Hello Phoenix"
  
    @moduledoc """
      This is where we would put any long form documentation or doctests.
    """
  
    def run(_args) do
      Mix.shell.info "Generating updater cache"
      generate()
      |> write_cache()
      Mix.shell.info "Done"
    end

    def write_cache(data) do
        data
            |> Poison.encode!
            |> (&File.write(base_path() <> "cache.json", &1, [:binary])).()
    end

    def read_cache() do
        File.read(base_path() <> "cache.json")
    end

    def get_cache() do
        with {:ok, content} <- read_cache() do
            Poison.decode!(content)
        else
            _ -> data = generate()
                write_cache(data)
                data
        end 
    end

    def base_path() do
        Application.get_env(:twglxy_api, Mix.Tasks.Twglxy.RegenerateUpdaterCache)[:base_path]
    end


    def generate() do
        File.ls!(base_path())
        |> Enum.filter(&(File.dir?(base_path() <> &1)))
        |> Enum.sort(&(&1 >= &2)) # Sort the versions descending
        |> Enum.map(fn v ->
            %{version: v, files: generate_version_info(v)}
        end)
    end

    def generate_version_info(version) do
        # First check the versions
        
        with oss <- ["osx", "win32", "linux"] do
            oss 
            |> Enum.reduce(Map.new(), fn os, acc ->
                Map.put(acc, os, generate_version_os(version, os))
                end
            )
        end
    end


    def generate_version_os(version, os) do
        dir = base_path() <> "#{version}/#{os}/"
        generate_directory_info(dir, dir)
    end


    def generate_directory_info(base, dir) do
        if File.dir?(dir) do
            File.ls!(dir)
                |> Enum.map(fn p -> dir <> p end)
                |> Enum.reduce(Map.new(), fn p, acc ->             
                        if (File.dir?(p)) do
                            Map.merge(generate_directory_info(base, p <> "/"), acc)
                        else
                            rel_p = String.slice(p, String.length(base)..-1)
                            Map.put(acc, rel_p, generate_file_info(p))
                        end
                    end
               )
        else 
            %{}
        end        
    end


    def crc32(str) do
        :erlang.crc32(str)
    end


    def generate_file_info(path) do
        with %{:size => size} <- File.stat!(path),
             content <- File.read!(path) do
            %{crc32: crc32(content), size: size}
         end
    end
  end
  